# README #
Python Script to create a test geodatabase with GNSS Metadata Fields and Domains


### How do I get set up? ###

* Dependencies - Python 2.7+, arcpy
* Deployment instructions - Download the script to a directory, Make sure you have write privileges on the directory, run the script. It will create a geodatabase called GPSTesting.gdb with a feature class with GNSS metadata fields in it.